# Banjo Kaboom's notes

Created this project to get up to date with the latest React, Webpack, NPM and related features.
I considered using the Typescript version of React, but everything I read and everything I'm comfortable with says that React is best to be written with javascript, not Typescript.

Followed latest instructions to install/create a react app using the script described here: https://reactjs.org/docs/create-a-new-react-app.html

Shortly after running the script, I did run `npm run eject` (see below) because I personally like to try to understand the configurations and set up as part of a React app. I also haven't played at this point with any Javascript unit testing frameworks (as I have been primarily in Magento working in PHP lately), and create-react-app starts you off with Jest, so I'm eager to get comfortable with that.

It appears the out of the box configurations for webpack, Jest and the npm commands are sufficient enough to handle everything and anything I throw at it, including SASS. Therefore, I don't at this moment see a benefit to ejecting the code as I did unless you too want to monkey around in the configs to get an understanding for them.

## Notes for newbies

Jest - this is my first time using Jest, and it actually is very easy. In case you can't figure it out, all you have to do is create a file that ends in .test.js or .spec.js, or put all your tests in a __tests__ folder. See the Jest documentation below. For this practice exercise, I simple used the .test.js format. The tests are simple right now, but you can see how they can be adapted to test mounting, unmounting and updates to the state.

You'll notice a lot of ES6/newer Javascript notation like such as import statements and arrow functions. You may be asking "how does this work in standard browsers that don't support that syntax?" Well, that's because it all gets transpiled to ES5 to make sure that the code works properly in standard browsers like IE, Firefox and Chrome.

Some notes on the syntax:

```
import React, { Component } from 'react';
```
This will import the 'default export' React object from the node_modules package 'react'. This will also import the Component object from React, which is not a default export, but is in fact exported by the 'react' module. In short, no {} means default export, and using {} exports other objects that are not the default.

```
import * as serviceWorker from './serviceWorker';
```
This imports everything from serviceWorker, and each function can be called from the serviceWorker object. In other words, it says "import everything from './serviceWorker' and let me call everything from an object called serviceWorker".

```
const {hex} = this.state;
```
Shorthand for getting variables out of an object. This is simply saying to get the variable hex out of this.state. This is the equivalent of saying 'var hex = this.state.hex'.

## Resources for things I mentioned

- https://reactjs.org/docs/create-a-new-react-app.html - Getting Started
- https://reactjs.org/docs/react-component.html  - Component Lifecycle
- https://reactjs.org/docs/typechecking-with-proptypes.html - PropTypes
- https://jestjs.io/ - Jest

--------------------------------------------
# Original README from running npx create-react-app

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br>
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (Webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

### Code Splitting

This section has moved here: https://facebook.github.io/create-react-app/docs/code-splitting

### Analyzing the Bundle Size

This section has moved here: https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size

### Making a Progressive Web App

This section has moved here: https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app

### Advanced Configuration

This section has moved here: https://facebook.github.io/create-react-app/docs/advanced-configuration

### Deployment

This section has moved here: https://facebook.github.io/create-react-app/docs/deployment

### `npm run build` fails to minify

This section has moved here: https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify
