import React, { Component } from 'react'; // React is needed to do anything with React, and the Component import is so that we can literally create components.
import './styles/styles.scss'; // Webpack is cool in that it will automatically pull in any static assets upon compiling the code with npm start or npm build.
import Thing from './components/Thing'; // Importing the component to use.

class App extends Component {
  // Just like Java or any other object-oriented programming language, the constructor runs when the component is instantiated
  // This is where you also instantiate any variables as well as the state of the component
  constructor(props) {
    super(props);
    this.state = {};
  }

  // Mounting is the equivalent of rendering the first time. When a component mounts, it has been created.
  componentDidMount() {

  }

  // Simply a way to manually tell the component if and when it should re-render because of an update to the state
  shouldComponentUpdate(nextProps, nextState) {
    return true;
  }

  // Simply a way to snapshot the previous state and properties before updating the component
  getSnapshotBeforeUpdate(prevProps, prevState) {
    return null;
  }

  // Updating is just that, on subsequent changes to the state of this component, it will update and re-render
  componentDidUpdate(prevProps, prevState, snapshot) {

  }

  // When a component is going to be removed from the view, this will trigger
  componentWillUnmount() {

  }

  // Any time an error is thrown within the component rendering process, you can add code here to do whatever you want with the error.
  componentDidCatch(error, info) {
    
  }

  // This is the key for all React components. Render is executed when mounting or updating a component, every time the state is updated, etc.
  render() {
    /**
     * This render example is very straightforward. I am using the "Thing" component and simple plopping it on the page.
     * One thing to note is that I am passing a property to the Thing component, called "componentName".
     * React doesn't use any kind of property verification, so you can pass whatever you want as a property and it can be read by the component.
     * However, you can use something called PropTypes to validate the types and names of the properties passed through.
     */
    return (
      <div>
        <h1>Thing</h1>
        <Thing componentName="Bob" />
      </div>
    );
  }
}

// This export represents the component name as a variable that can be used and imported into other components.
export default App;
