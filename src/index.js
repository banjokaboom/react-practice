/**
 * This is the entry point for the whole application.
 * Renders the App component from App.js.
 * This file shouldn't ever be modified unless you want to use routing to direct users to different apps via different entry components.
 * However, React is typically used for smaller scale single-page-apps, bigger than Knockout but smaller than Angular.
 */

import React from 'react'; // Needed to do anything, really
import ReactDOM from 'react-dom'; // Required to be able to render the app
import App from './App'; // the component you want to use on the page. You can have multiple.
import * as serviceWorker from './serviceWorker'; // review the comments in serviceWorker.js to understand what this does.

ReactDOM.render(<App />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
