import React, { Component } from 'react'; // React is needed to do anything with React, and the Component import is so that we can literally create components.

class Thing extends Component {
  // Just like Java or any other object-oriented programming language, the constructor runs when the component is instantiated
  // This is where you also instantiate any variables as well as the state of the component
  constructor(props) {
    super(props);

    // In this case, we are saying that this component will have a default state containing a hex string and a text string, both of which are blank to start
    this.state = {
      hex: '',
      text: ''
    };
    
    // Any custom helper functions that are used within this component need to be bound to 'this'
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  // Mounting is the equivalent of rendering the first time. When a component mounts, it has been created.
  componentDidMount() {

  }

  // Simply a way to manually tell the component if and when it should re-render because of an update to the state
  shouldComponentUpdate(nextProps, nextState) {
    return true;
  }

  // Simply a way to snapshot the previous state and properties before updating the component
  getSnapshotBeforeUpdate(prevProps, prevState) {
    return null;
  }

  // Updating is just that, on subsequent changes to the state of this component, it will update and re-render
  componentDidUpdate(prevProps, prevState, snapshot) {

  }

  // When a component is going to be removed from the view, this will trigger
  componentWillUnmount() {

  }

  // Any time an error is thrown within the component rendering process, you can add code here to do whatever you want with the error.
  componentDidCatch(error, info) {
    
  }
  
  /**
   * Helper funtion to be used by the submit button to convert the text into a hex color.
   * Functions in react typically use an 'event' parameter as the variable passed in.
   * This is the same as the Javascript object 'event' for an HTML event where you would use event.target.value to get the value of an input.
   * You can also create functions like a normal JS function and pass in whatever you want!
   */
  handleSubmit(e) {
    let hexStr = '';
    let strChars = this.state.text.split('');

    for(let index = 0; index < strChars.length; index++) {
        hexStr += (strChars[index].charCodeAt(0)*111111).toString(16);
    }

    this.setState({text: '', hex: hexStr.substring(0,6)});
  }

  // This is the key for all React components. Render is executed when mounting or updating a component, every time the state is updated, etc.
  render() {
    // The render function is just like any other function, so you can set and create variables to be used throughout the render process, each time.
    const {hex} = this.state;
    let resultStyles;
    
    if(hex !== '') {
      resultStyles = {
        backgroundColor: '#' + hex,
        padding: '1em'
      }
    }

    /**
     * React uses JSX, which essentially is HTML embedded within javascript code.
     * It doesn't require you to create the HTML in a string, you can just write HTML.
     * 
     * Here I am creating a div element with the class of 'component'. React uses specific attributes to represent HTML attributes.
     * className is the equivalent of class. htmlFor is the equivalent of 'for' on a label element.
     * 
     * Syntax for using variables within JSX is also different from normal Javascript.
     * You would use {} and put the variable you want to utilize within the braces.
     * You can see below where I am calling this.props to display a property passed to the object, and this.state to do the same for a value in the state.
     *    
     * Another fun thing is the ability to create functions right within the onChange event (see below).
     * Using an arrow function and passing in the event, I can update the state of this component without calling a helper function.
     */ 
    return (
      <div className="component">
        Test Component {this.props.componentName}
        <div>
          <label htmlFor="stringToConvert">Enter text to convert to hex!</label>
          <input id="stringToConvert" type="text" value={this.state.text} onChange={(e) => this.setState({text: e.target.value, hex: '' })} />
          <button onClick={this.handleSubmit}>Submit!</button>
        </div>
        <div style={resultStyles}></div>
      </div>
    );
  }
}

// This export represents the component name as a variable that can be used and imported into other components.
export default Thing;
